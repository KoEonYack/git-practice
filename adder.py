def adder(a, b):
	return a + b

if __name__ == "__main__":
	a, b = map(int, input().split(" "))
	print(adder(a, b))
